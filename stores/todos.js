import _ from 'lodash';
import { writable, get } from 'svelte/store';

function write(data) {
  localStorage.setItem('todos', JSON.stringify(data));
}

function read() {
  const json = localStorage.getItem('todos') || '{"items":[]}';
  return JSON.parse(json);
}

function createTodos() {
  const store = writable(read());

  function update(updater) {
    store.update((data) => {
      const newData = updater(data);
      write(newData);
      return newData;
    });
  }

  return {
    subscribe: store.subscribe,
    create: (task) => update((current) => _.update(
      current,
      ['items'],
      (items) => [...items, task],
    )),
    delete: (index) => update(current => _.update(
      current,
      ['items'],
      (items) => {
        items.splice(index, 1);
        return items;
      },
    )),
    rename: (index, title) => update((current) => _.set(
      current,
      ['items', index, 'title'],
      title,
    )),
    change: (index, done) => update(current => _.set(
      current,
      ['items', index, 'done'],
      done,
    )),
  };
}

const todos = createTodos();

export default todos;