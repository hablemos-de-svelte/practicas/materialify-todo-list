# Todo list
Review de [Svelte Materialify](https://svelte-materialify.vercel.app/) por Raymundo de [Hablemos de Svelte](https://t.me/hablemos_de_svelte)

Hice un todo list para probar algunas cosas basicas de esta librería, a continuación les dejo algunas anotaciones

## Run
```sh
npm i -g pnpm # instalación de pnpm
pnpm i
pnpm dev
```

## Review

Cosas buenas
- Tiene un vasto repertorio componentes
- La documentación de los componentes es buena

Mi experiencia
- No encontré documentación para instalar el paquete de iconos, lo solucioné usando un cdn (googled)
- No encontré como hacer focus al componente TextField (parece que no hay soporte de esto)
- El componente Checkbox no tiene el comportamiento on/off de tipo boolean
  - Pude hacer un componente Checkbox sin esfuerzo usando los componentes existentes
